import 'package:ecomerce/models/product_model.dart';
import 'package:ecomerce/modules/home_module/cubit.dart';
import 'package:ecomerce/modules/home_module/state.dart';
import 'package:ecomerce/widgets/svg_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class ProductItem extends StatelessWidget {
  final Product product;

  const ProductItem({super.key, required this.product});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      builder: (context, state) {
        final cubit = context.watch<HomeCubit>();

        final isFavorite = state.selectedProducts.contains(product);

        return Card(
          elevation: 4,
          color: const Color.fromRGBO(255, 255, 255, 1),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(11)),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 9, vertical: 9),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (product.image != null)
                  Center(
                    child: SizedBox(
                      height: 111,
                      child: Image.network(product.image!),
                    ),
                  ),
                const SizedBox(height: 4),
                Flexible(
                  child: ShaderMask(
                    shaderCallback: (Rect bounds) {
                      return const LinearGradient(
                        begin: Alignment.center,
                        end: Alignment.centerRight,
                        colors: [
                          Color.fromRGBO(252, 252, 252, 1),
                          Color.fromRGBO(252, 252, 252, 0),
                        ],
                      ).createShader(bounds);
                    },
                    child: Text(
                      product.title ?? '',
                      maxLines: 1,
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      style: const TextStyle(
                        fontSize: 12,
                        color: Color.fromRGBO(49, 49, 49, 1),
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
                RatingBarIndicator(
                  rating: product.rating?.rate ?? 0,
                  itemBuilder: (context, index) => Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 1),
                    child: Container(
                      decoration: BoxDecoration(
                        color: const Color.fromRGBO(255, 214, 70, 1),
                        borderRadius: BorderRadius.circular(50),
                      ),
                      height: 4.71,
                      width: 4.71,
                    ),
                  ),
                  unratedColor: const Color.fromRGBO(230, 230, 230, 1),
                  itemCount: 5,
                  itemSize: 12,
                ),
                const SizedBox(height: 3),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('\$ ${product.price.toString()}'),
                    GestureDetector(
                      onTap: () {
                        if (isFavorite) {
                          cubit.removeSelectedProduct(product);
                        } else {
                          cubit.addSelectedProduct(product);
                        }
                      },
                      child: isFavorite ? const SvgIcon('favorite_active') : const SvgIcon('favorite_disable'),
                    )
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

// class ProductItem extends StatelessWidget {
//   final Product product;
//
//   const ProductItem({Key? key, required this.product}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return BlocBuilder<HomeCubit, HomeState>(
//       builder: (context, state) {
//         final cubit = context.watch<HomeCubit>();
//
//         final isFavorite = state.selectedProducts.contains(product);
//
//         return Card(
//           elevation: 4,
//           color: const Color.fromRGBO(255, 255, 255, 1),
//           shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(11),
//           ),
//           child: Padding(
//             padding: const EdgeInsets.symmetric(horizontal: 9, vertical: 9),
//             child: Column(
//               mainAxisSize: MainAxisSize.max,
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 if (product.image != null)
//                   Center(
//                     child: SizedBox(
//                       height: 111,
//                       child: Image.network(product.image!),
//                     ),
//                   ),
//                 const SizedBox(height: 4),
//                 Flexible(
//                   child: Text(
//                     product.title ?? '',
//                     overflow: TextOverflow.ellipsis,
//                     style: const TextStyle(
//                       fontSize: 12,
//                       color: Color.fromRGBO(49, 49, 49, 1),
//                       fontWeight: FontWeight.w600,
//                     ),
//                   ),
//                 ),
//                 RatingBarIndicator(
//                   rating: product.rating?.rate ?? 0,
//                   itemBuilder: (context, index) => Padding(
//                     padding: const EdgeInsets.symmetric(horizontal: 1),
//                     child: Container(
//                       decoration: BoxDecoration(
//                         color: const Color.fromRGBO(255, 214, 70, 1),
//                         borderRadius: BorderRadius.circular(50),
//                       ),
//                       height: 4.71,
//                       width: 4.71,
//                     ),
//                   ),
//                   unratedColor: const Color.fromRGBO(230, 230, 230, 1),
//                   itemCount: 5,
//                   itemSize: 12,
//                 ),
//                 const SizedBox(height: 3),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     Text('\$ ${product.price.toString()}'),
//                     IconButton(
//                       icon: Icon(
//                         isFavorite ? Icons.favorite : Icons.favorite_border,
//                         color: isFavorite ? Colors.red : null,
//                       ),
//                       onPressed: () {
//                         if (isFavorite) {
//                           cubit.removeSelectedProduct(product);
//                         } else {
//                           cubit.addSelectedProduct(product);
//                         }
//                       },
//                     ),
//                   ],
//                 ),
//               ],
//             ),
//           ),
//         );
//       },
//     );
//   }
// }
