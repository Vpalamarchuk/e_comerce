import 'package:ecomerce/widgets/svg_icon.dart';
import 'package:flutter/material.dart';

class SearchField extends StatelessWidget {
  const SearchField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 33.87,
      width: 287,
      child: TextField(
        cursorHeight: 18,
        cursorColor: Colors.black,
        decoration: InputDecoration(
          filled: true,
          contentPadding: const EdgeInsets.all(10),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(21),
            borderSide: BorderSide.none,
          ),
          suffixIcon: const Icon(Icons.search),
          suffixIconColor: const Color.fromRGBO(160, 160, 160, 1),
        ),
      ),
    );
  }
}
