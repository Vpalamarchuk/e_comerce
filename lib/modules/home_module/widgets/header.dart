import 'package:ecomerce/modules/home_module/widgets/search_field.dart';
import 'package:ecomerce/widgets/svg_icon.dart';
import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  const Header({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        GestureDetector(
          onTap: () => Scaffold.of(context).openDrawer(),
          child: const SvgIcon('menu'),
        ),
        const SizedBox(width: 27),
        const Expanded(child: SearchField()),
      ],
    );
  }
}
