import 'package:ecomerce/core/app_bloc.dart';
import 'package:ecomerce/modules/home_module/cubit.dart';
import 'package:ecomerce/modules/home_module/state.dart';
import 'package:ecomerce/modules/home_module/views/products_view.dart';
import 'package:ecomerce/modules/home_module/widgets/header.dart';
import 'package:ecomerce/utils/unfocus_wrapper.dart';
import 'package:ecomerce/widgets/svg_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatelessWidget {
  static const path = 'home';

  const HomeScreen({Key? key}) : super(key: key);

  void _logout(BuildContext context) {
    AppBloc.appCubit.logout();
    Navigator.of(context).popUntil((route) => route.settings.name == '/');
  }

  @override
  Widget build(BuildContext context) {
    return UnfocusWrapper(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          automaticallyImplyLeading: false,
          centerTitle: false,
          title: const Header(),
        ),
        drawer: Drawer(
          child: Column(
            children: [
              const DrawerHeader(
                child: Text('eComerce', style: TextStyle(fontSize: 22)),
              ),
              const Spacer(),
              ListTile(
                title: const Text('Log Out'),
                leading: const Icon(Icons.logout),
                onTap: () => _logout(context),
              ),
            ],
          ),
        ),
        body: BlocProvider(
          create: (context) => HomeCubit()..init(),
          child: BlocBuilder<HomeCubit, HomeState>(builder: (context, state) {
            final cubit = context.watch<HomeCubit>();

            if (state.isLoading) {
              return const Center(child: CircularProgressIndicator());
            }

            return RefreshIndicator(
              onRefresh: () => cubit.fetchProducts(),
              child: Stack(
                children: [
                  SingleChildScrollView(
                    controller: cubit.scrollController,
                    child: const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 26),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 26),
                          Text(
                            'Special offers',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Text(
                            'The best prices',
                            style: TextStyle(
                              fontSize: 12,
                              color: Color.fromRGBO(49, 49, 49, 1),
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          SizedBox(height: 15),
                          ProductsView(),
                        ],
                      ),
                    ),
                  ),
                  if (cubit.isProductSelectedNotEmpty)
                    Positioned(
                      left: MediaQuery.of(context).size.width / 2.35,
                      top: MediaQuery.of(context).size.height / 1.3,
                      child: const SvgIcon('favorites_basket'),
                    )
                ],
              ),
            );
          }),
        ),
      ),
    );
  }
}
