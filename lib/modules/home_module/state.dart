import 'package:ecomerce/models/product_model.dart';

class HomeState {
  final bool isLoading;
  final List<Product> products;
  final List<Product> selectedProducts;


  const HomeState({
    this.isLoading = false,
    this.products = const [],
    this.selectedProducts = const [],
  });

  HomeState copyWith({
    bool? isLoading,
    List<Product>? products,
    List<Product>? selectedProducts,
  }) {
    return HomeState(
      isLoading: isLoading ?? this.isLoading,
      products: products ?? this.products,
      selectedProducts: selectedProducts ?? this.selectedProducts,

    );
  }
}
