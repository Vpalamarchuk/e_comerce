import 'package:ecomerce/modules/home_module/cubit.dart';
import 'package:ecomerce/modules/home_module/widgets/product_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProductsView extends StatelessWidget {
  const ProductsView({super.key});

  @override
  Widget build(BuildContext context) {
    final cubit = context.watch<HomeCubit>();
    final state = cubit.state;

    return GridView.builder(
      controller: cubit.scrollController,
      shrinkWrap: true,
      itemCount: state.products.length,
      itemBuilder: (context, index) => ProductItem(product: state.products[index]),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: MediaQuery.of(context).size.width / (MediaQuery.of(context).size.height / 1.8),
      ),
    );
  }
}
