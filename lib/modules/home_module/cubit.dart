import 'package:ecomerce/api/product_api.dart';
import 'package:ecomerce/models/product_model.dart';
import 'package:ecomerce/modules/home_module/state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(const HomeState());

  List<Product>? selectedProducts;
  final scrollController = ScrollController();

  bool get isProductSelectedNotEmpty => state.selectedProducts.isNotEmpty;

  void init() => fetchProducts();

  Future<void> fetchProducts() async {
    emit(state.copyWith(isLoading: true));
    try {
      final products = await ProductApi.fetchProducts();

      emit(state.copyWith(isLoading: false, products: products));
    } catch (e, s) {
      emit(state.copyWith(isLoading: false));
    }
  }

  void addSelectedProduct(Product product) {
    emit(state.copyWith(selectedProducts: [...state.selectedProducts, product]));
  }

  void removeSelectedProduct(Product product) {
    final selectedProducts = state.selectedProducts.where((e) => e != product).toList();

    emit(state.copyWith(selectedProducts: selectedProducts));
  }
}
