import 'package:ecomerce/modules/login_module/cubit.dart';
import 'package:ecomerce/utils/validators.dart';
import 'package:ecomerce/widgets/text_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({super.key});

  @override
  Widget build(BuildContext context) {
    final cubit = context.watch<LoginCubit>();

    return Form(
      onChanged: cubit.validateForm,
      child: Column(
        children: [
          ActionInput(
            controller: cubit.userNameController,
            hint: 'User name',
            validator: (value) => Validators.userName(value),
          ),
          const SizedBox(height: 31),
          ActionInput(
            controller: cubit.passwordController,
            hint: 'Password',
            obscure: true,
            showObscureButton: true,
            validator: (value) => Validators.password(value),
          ),
        ],
      ),
    );
  }
}
