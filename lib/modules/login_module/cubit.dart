import 'package:ecomerce/core/app_bloc.dart';
import 'package:ecomerce/modules/login_module/state.dart';
import 'package:ecomerce/utils/validators.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit() : super( const LoginState());

  final userNameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Future<void> close() {
    userNameController.dispose();
    passwordController.dispose();
    return super.close();
  }

  void validateForm() {
    final errors = [
      Validators.userName(userNameController.text),
      Validators.password(passwordController.text),
    ];

    final emptyControllers = userNameController.text.isNotEmpty &&
        passwordController.text.isNotEmpty;

    final isValid = errors.every((e) => e == null) && emptyControllers;

    emit(state.copyWith(isValid: isValid));
  }

  Future<void> login() async {
    emit(state.copyWith(isLoading: true));

    await AppBloc.appCubit.authenticate(
      userNameController.text,
      passwordController.text,
    );

    emit(state.copyWith(isLoading: false));
  }
}
