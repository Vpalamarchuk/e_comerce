class LoginState {
  final bool isValid;
  final bool isLoading;

  const LoginState({
    this.isValid = false,
    this.isLoading = false,
  });

  LoginState copyWith({
    bool? isValid,
    bool? isLoading,
  }) {
    return LoginState(
      isValid: isValid ?? this.isValid,
      isLoading: isLoading ?? this.isLoading,
    );
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is LoginState && other.isValid == isValid && other.isLoading == isLoading;
  }

  @override
  int get hashCode => isValid.hashCode ^ isLoading.hashCode;
}
