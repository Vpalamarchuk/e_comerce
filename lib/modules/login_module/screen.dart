import 'package:ecomerce/modules/login_module/cubit.dart';
import 'package:ecomerce/modules/login_module/state.dart';
import 'package:ecomerce/modules/login_module/views/login_form.dart';
import 'package:ecomerce/utils/unfocus_wrapper.dart';
import 'package:ecomerce/widgets/expanded_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatelessWidget {
  static const path = '/';

  const LoginScreen({Key? key}) : super(key: key);

  void _login(BuildContext context) => context.read<LoginCubit>().login();

  @override
  Widget build(BuildContext context) {
    return UnfocusWrapper(
      child: BlocProvider(
        create: (context) => LoginCubit(),
        child: BlocBuilder<LoginCubit, LoginState>(
          builder: (context, state) {
            return Scaffold(
              body: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 22),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset('assets/png/logo.png', height: 60),
                    const SizedBox(height: 81),
                    const LoginForm(),
                    const SizedBox(height: 8.5),
                    const Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        'Forgot password',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color.fromRGBO(21, 138, 138, 1),
                        ),
                      ),
                    ),
                    const SizedBox(height: 35),
                    AppButton(
                      'Log in',
                      disabled: !state.isValid,
                      isLoading: state.isLoading,
                      onTap: () => _login(context),
                    )
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
