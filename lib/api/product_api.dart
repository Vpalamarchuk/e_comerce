import 'package:ecomerce/core/http_client.dart';
import 'package:ecomerce/models/product_model.dart';

class ProductApi {
  static Future<List<Product>> fetchProducts() async {
    final response = await HttpClient.api.get('products');

    return (response.data as List).map((p) => Product.fromJson(p as Map<String, dynamic>)).toList();
  }
}
