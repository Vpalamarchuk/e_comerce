
import 'package:ecomerce/core/http_client.dart';

class UserApi {
  static Future<String> login(String userName, String password) async {
    final response = await HttpClient.api.post(
      'auth/login',
      data: {
        "username": userName,
        "password": password,
      },
    );

    return response.data['token'] as String;
  }
}
