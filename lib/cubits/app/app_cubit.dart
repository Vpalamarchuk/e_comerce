import 'dart:async';

import 'package:ecomerce/api/user_api.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

part 'app_state.dart';

class AppCubit extends Cubit<AppState> with HydratedMixin {
  AppCubit() : super(const AppState());

  void onStartApp() {}

  void onCloseApp() {}

  Future<void> authenticate(String userName, String password) async {
    try {
      final token = await UserApi.login(userName, password);
      emit(state.copyWith(token: token));
    } catch (_) {
      //  TODO: logic from error
    }
  }

  Future<void> logout() async => emit(const AppState(token: null));

  @override
  Map<String, dynamic> toJson(AppState state) => state.toJson();

  @override
  AppState fromJson(Map<String, dynamic> json) => AppState.fromJson(json);
}
