part of 'app_cubit.dart';

class AppState {
  final String? token;

  const AppState({
    this.token,
  });

  AppState copyWith({
    String? token,
  }) {
    return AppState(
      token: token ?? this.token,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'token': token,
    };
  }

  factory AppState.fromJson(Map<String, dynamic> map) {
    return AppState(
      token: map['token'],
    );
  }
}
