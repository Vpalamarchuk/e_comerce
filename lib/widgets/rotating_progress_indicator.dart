import 'dart:math';

import 'package:flutter/material.dart';

class RotatingProgressIndicator extends StatefulWidget {
  final double? strokeWidth;
  final double? size;

  const RotatingProgressIndicator({
    Key? key,
    this.size,
    this.strokeWidth,
  }) : super(key: key);

  @override
  State<RotatingProgressIndicator> createState() =>
      _RotatingProgressIndicatorState();
}

class _RotatingProgressIndicatorState extends State<RotatingProgressIndicator>
    with TickerProviderStateMixin {
  late final _controller = AnimationController(
    vsync: this,
    duration: const Duration(milliseconds: 600),
  )..repeat();

  @override
  void initState() {
    _controller.addListener(() => setState(() {}));
    super.initState();
  }

  @override
  void dispose() {
    _controller.removeListener(() => setState(() {}));
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = widget.size ?? 36;
    return SizedBox(
      width: size,
      height: size,
      child: Transform.rotate(
        angle: 2 * pi * _controller.value,
        child: const CircularProgressIndicator(
          strokeWidth: 2,
          color: Colors.white,
        ),
      ),
    );
  }
}
