import 'package:ecomerce/widgets/rotating_progress_indicator.dart';
import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  final Color? color;
  final String text;
  final bool disabled;
  final Color? textColor;
  final void Function() onTap;
  final bool? isLoading;

  const AppButton(
    this.text, {
    required this.onTap,
    this.color,
    this.textColor,
    this.disabled = false,
    Key? key,
    this.isLoading,
  }) : super(key: key);

  Color get _backgroundColor {
    if (disabled) return const Color.fromRGBO(212, 212, 219, 1);
    return const Color.fromRGBO(21, 138, 138, 1);
  }

  Color get _textColor {
    if (disabled) return const Color.fromRGBO(157, 157, 157, 1);

    return const Color.fromRGBO(243, 243, 243, 1);
  }

  void _handleTap(BuildContext context) {
    FocusScope.of(context).unfocus();
    onTap.call();
  }

  Tween<double> _getTween() {
    if (isLoading == null) return Tween<double>(begin: 0, end: 0);
    if (isLoading!) return Tween<double>(begin: 0, end: 1);
    return Tween<double>(begin: 1, end: 0);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 43,
      width: double.infinity,
      child: ElevatedButton(
        onPressed: !disabled ? () => _handleTap(context) : null,
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
        ).merge(
          ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(_backgroundColor),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(text, style: TextStyle(fontSize: 14, color: _textColor)),
            TweenAnimationBuilder<double>(
              tween: _getTween(),
              duration: const Duration(milliseconds: 200),
              builder: (content, value, _) {
                if (value == 0) return const SizedBox();

                return Opacity(
                  opacity: value,
                  child: Padding(
                    padding: EdgeInsets.only(left: 8 * value),
                    child: RotatingProgressIndicator(size: 18 * value),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
