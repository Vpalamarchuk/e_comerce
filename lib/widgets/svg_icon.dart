import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SvgIcon extends StatelessWidget {
  final double? width;
  final double? height;
  final String iconName;
  final BoxFit fit;

  const SvgIcon(
    this.iconName, {
    this.width,
    this.height,
    this.fit = BoxFit.contain,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      'assets/svg/$iconName.svg',
      width: width,
      height: height,
      fit: fit,
    );
  }
}
