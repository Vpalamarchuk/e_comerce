import 'dart:async';

import 'package:flutter/material.dart';

class ActionInput extends StatefulWidget {
  final bool obscure;
  final String? hint;
  final TextStyle? hintStyle;
  final String? error;
  final Widget? suffixIcon;
  final void Function()? onTap;
  final bool showObscureButton;
  final void Function(bool)? onValidated;
  final TextInputAction? textInputAction;
  final TextEditingController? controller;
  final String? Function(String?)? validator;

  const ActionInput({
    Key? key,
    this.hint,
    this.error,
    this.onTap,
    this.validator,
    this.controller,
    this.suffixIcon,
    this.onValidated,
    this.textInputAction,
    this.obscure = false,
    this.showObscureButton = false,
    this.hintStyle,
  }) : super(key: key);

  @override
  State<ActionInput> createState() => ActionInputState();
}

class ActionInputState extends State<ActionInput> {
  late bool _obscure = widget.obscure;
  Timer? _validatorTimer;
  late String? _errorText = widget.error;
  late final _controller = widget.controller ?? TextEditingController();

  @override
  void initState() {
    _controller.addListener(validator);
    super.initState();
  }

  @override
  void dispose() {
    _controller.removeListener(validator);
    super.dispose();
  }

  void _toggleObscure() {
    setState(() => _obscure = !_obscure);
  }

  void _validate() {
    final value = _controller.text;
    final error = widget.validator!(value);
    if (_errorText == error) return;
    setState(() => _errorText = error);
    widget.onValidated?.call(_errorText?.isEmpty ?? true);
  }

  void validator() {
    if (widget.validator == null) return;
    if (_validatorTimer?.isActive ?? false) _validatorTimer!.cancel();

    if (_errorText != null) setState(() => _errorText = null);

    _validatorTimer = Timer(
      const Duration(milliseconds: 1500),
      () => WidgetsBinding.instance.addPostFrameCallback((_) => _validate()),
    );
  }

  @override
  Widget build(BuildContext context) {
    final isValid = _errorText?.isEmpty ?? true;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextFormField(
          onTap: widget.onTap,
          obscureText: _obscure,
          controller: widget.controller,
          cursorColor: const Color.fromRGBO(21, 138, 138, 1),
          style: const TextStyle(fontSize: 14),
          decoration: InputDecoration(
            hintText: widget.hint,
            border: InputBorder.none,
            hintStyle: widget.hintStyle,
            contentPadding:
                const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
            suffixIcon: widget.showObscureButton
                ? _ObscureButton(obscure: _obscure, onTap: _toggleObscure)
                : widget.suffixIcon,
          ),
        ),
        Center(
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: CustomPaint(painter: _LinePainter()),
          ),
        ),
        if (!isValid) ...[
          const SizedBox(height: 4),
          Text(
            _errorText!,
            style: const TextStyle(fontSize: 10, color: Colors.red),
          ),
        ]
      ],
    );
  }
}

class _ObscureButton extends StatelessWidget {
  final bool obscure;
  final void Function()? onTap;

  const _ObscureButton({
    Key? key,
    this.onTap,
    required this.obscure,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: AnimatedSwitcher(
        duration: const Duration(milliseconds: 200),
        child: Icon(
          obscure ? Icons.visibility_off_outlined : Icons.visibility_outlined,
          key: ValueKey(obscure),
          color: const Color.fromRGBO(157, 157, 157, 1),
        ),
      ),
    );
  }
}

class _LinePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = const Color.fromRGBO(21, 138, 138, 1)
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 2;

    canvas.drawLine(
      Offset(1.75, size.height / 2),
      Offset(size.width - 1.75, size.height / 2),
      paint,
    );

    canvas.drawLine(
      const Offset(3, 0),
      Offset(1.75, size.height - 1),
      paint,
    );

    canvas.drawLine(
      Offset(size.width - 3, 0),
      Offset(size.width - 1.75, size.height - 1),
      paint,
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}
