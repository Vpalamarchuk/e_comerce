import 'package:ecomerce/core/app_bloc.dart';
import 'package:ecomerce/core/http_client.dart';
import 'package:ecomerce/widgets/splash_lifecycle_overlay.dart';
import 'package:ecomerce/widgets/splash_screen.dart';
import 'package:flutter/material.dart';

class SetupApp extends StatefulWidget {
  final Widget Function(BuildContext) builder;

  const SetupApp({required this.builder, Key? key}) : super(key: key);

  @override
  _SetupAppState createState() => _SetupAppState();
}

class _SetupAppState extends State<SetupApp> {
  final Future<void> _setupFuture = _setup();

  static Future<void> _setup() async {
    AppBloc.init();
    await HttpClient.init();
    AppBloc.appCubit.onStartApp();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _setupFuture,
      builder: (context, snapshot) {
        return SplashLifecycleOverlay(
          splashBuilder: (_) => const SplashScreen(),
          child: AppBloc.appBuilder(widget.builder),
        );
      },
    );
  }
}
