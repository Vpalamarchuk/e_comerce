import 'package:dio/dio.dart';
import 'package:ecomerce/utils/interceptors.dart';
import 'package:flutter/foundation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class HttpClient {
  static late Dio api;

  static Future<void> init() async {
    api = Dio();
    api.options.baseUrl = 'https://fakestoreapi.com/';

    api.interceptors.add(ErrorInterceptor());

    if (kDebugMode) {
      api.interceptors.add(
        PrettyDioLogger(
          error: true,
          requestBody: true,
          responseBody: true,
        ),
      );
    }
  }
}
