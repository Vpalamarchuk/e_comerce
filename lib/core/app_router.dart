// ignore_for_file: prefer_const_constructors

import 'package:ecomerce/modules/home_module/screen.dart';
import 'package:ecomerce/modules/login_module/screen.dart';
import 'package:ecomerce/utils/screen_transitions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppRouter {
  static Map<String, WidgetBuilder> routes = {
    LoginScreen.path: (context) => LoginScreen(),
    HomeScreen.path: (context) => HomeScreen(),
  };

  static Route onGenerateRoute(RouteSettings settings) {
    return CustomCupertinoRoute(
      settings: settings,
      builder: (context) => routes[settings.name]!(context),
    );
  }
}
