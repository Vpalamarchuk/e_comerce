class Validators {
  static String? userName(String? value) {
    if (value == null || value.isEmpty) return null;

    final regExp = RegExp(r"^[a-zA-Z0-9_]*$");

    return regExp.hasMatch(value) ? null : 'Invalid user name';
  }

  static String? password(String? value) {
    if ((value == null || value.isEmpty)) return null;
    if (value.length < 5) return 'Invalid password length min';

    final spaceRegExp = RegExp('[ ]');

    if (spaceRegExp.hasMatch(value)) {
      return 'Invalid password space';
    }

    return null;
  }
}
