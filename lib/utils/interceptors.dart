import 'package:dio/dio.dart';
import 'package:ecomerce/app.dart';
import 'package:flutter/material.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class ErrorInterceptor extends Interceptor {
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    _showDefaultError(err.response?.data);
    return super.onError(err, handler);
  }

  void _showDefaultError(String error) {
    showTopSnackBar(
      Overlay.of(App.context!),
      CustomSnackBar.error(message: error),
    );
  }
}
