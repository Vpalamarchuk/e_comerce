import 'package:ecomerce/modules/home_module/screen.dart';
import 'package:ecomerce/modules/login_module/screen.dart';
import 'package:ecomerce/setup.dart';
import 'package:ecomerce/widgets/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:google_fonts/google_fonts.dart';

import 'core/app_router.dart';
import 'cubits/app/app_cubit.dart';

class App extends StatelessWidget {
  static BuildContext? context;
  static final navigatorKey = GlobalKey<NavigatorState>();

  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return SetupApp(
      builder: (context) {
        return MaterialApp(
          theme: ThemeData(
            textTheme: GoogleFonts.poppinsTextTheme(
              Theme.of(context).textTheme,
            ),
          ),
          home: const _StartScreen(),
          navigatorKey: navigatorKey,
          debugShowCheckedModeBanner: false,
          onGenerateRoute: AppRouter.onGenerateRoute,
        );
      },
    );
  }
}

class _StartScreen extends StatefulWidget {
  const _StartScreen();

  @override
  State<_StartScreen> createState() => _StartScreenState();
}

class _StartScreenState extends State<_StartScreen> {
  bool _previewVisible = true;

  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(seconds: 3)).then(
      (_) => setState(() => _previewVisible = false),
    );
  }

  Widget _buildChild(BuildContext context) {
    final appState = context.watch<AppCubit>().state;

    if (appState.token == null) return const LoginScreen();
    return const HomeScreen();
  }

  @override
  Widget build(BuildContext context) {
    FlutterNativeSplash.remove();
    App.context = context;

    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 350),
      child: _previewVisible ? const SplashScreen() : _buildChild(context),
    );
  }
}
